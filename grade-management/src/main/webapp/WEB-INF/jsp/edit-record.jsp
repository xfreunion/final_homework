<%--
  Created by IntelliJ IDEA.
  User: kurodamaria
  Date: 2020/12/27
  Time: 17:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>添加成绩记录</title>
    <%--    使用bootstrap美化html--%>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<%--    引入jQuery--%>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js"></script>
</head>
<body>
    <h1>添加成绩记录</h1>
    <s:form modelAttribute="grade" action="SaveRecord">
<%--        复制id--%>
        <s:hidden path="id"/>

        <div class="mb-3">
            <label class="form-label" for="examLabelInput">考试名称</label>
            <s:input path="exam_label" id="examLabelInput" oninput="switchPageState()"/>
            <div hidden id="examLabelHelp" class="form-text">考试名称不能为空</div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="stuIdInput">学生学号</label>
            <s:input path="stu_id" id="stuIdInput" oninput="switchPageState()"/>
            <div hidden id="stuIdInputHelp" class="form-text">学号必须输入数字，且不能为空</div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="stuNameInput">学生姓名</label>
            <s:input path="stu_name" id="stuNameInput" oninput="switchPageState()"/>
            <div hidden id="stuNameInputHelp" class="form-text">姓名不能为空</div>
        </div>
        <div class="mb-3">
            <label class="form-label" for="scoreInput">学生成绩</label>
            <s:input path="score" id="scoreInput" oninput="switchPageState()"/>
            <div hidden id="scoreInputHelp" class="form-text">学生成绩 >= 0 且必须是数字，不能为空</div>
        </div>

        <button id="submitBtn" type="submit" class="btn btn-primary" disabled>提交</button>

    </s:form>
    <div class="list-group">
        <a href="${pageContext.request.contextPath}/" class="list-group-item list-group-item-action">返回</a>
    </div>
</body>
<script>
    // 检测是否可以提交
    // 1. 四个输入框都不能为空
    // 2. stuIdInput, scoreInput只能输入数字
    // 3. stuIdInput不能是小数
    // 如果输入错误则给出相应的提示
    function switchPageState() {
        // 检测学号输入是否是整数
        let isInteger = $('#stuIdInput').val().match(/^[0-9]+$/g);
        // 检测分数是否符合格式
        let isPositiveNumber = $('#scoreInput').val().match(/^[0-9]*\.?[0-9]+$/g);
        // 检测考试名称是否不为空
        let isLabelNotEmpty = $('#examLabelInput').val() !== "";
        // 检测学生姓名是否不为空
        let isNameNotEmpty = $('#stuNameInput').val() !== "";

        // 根据前面检测的结果显示相应的提示
        $("#examLabelHelp").prop('hidden', isLabelNotEmpty);
        $("#stuNameInputHelp").prop('hidden', isNameNotEmpty);
        $("#stuIdInputHelp").prop('hidden', isInteger);
        $("#scoreInputHelp").prop('hidden', isPositiveNumber);

        // 根据前面检测的结果决定是否启用提交按钮
        let shouldEnable = isInteger && isPositiveNumber && isLabelNotEmpty && isNameNotEmpty;
        $("#submitBtn").prop('disabled', !shouldEnable);
    }
</script>
</html>
