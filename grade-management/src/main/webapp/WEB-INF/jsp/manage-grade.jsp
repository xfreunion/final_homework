<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>成绩管理</title>

    <%--    使用bootstrap美化html--%>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div style="display: flex; justify-content: center">
    <h2>成绩列表</h2>
</div>
<div id="list-of-students">
    <table class="table table-bordered table-hover" id="sort-table" data-search="true">
        <caption></caption>
        <thead>
        <tr>
            <th scope="col">学号</th>
            <th scope="col">姓名</th>
            <th scope="col">考试</th>
            <th scope="col">成绩</th>
            <th scope="col">操作</th>
        </tr>
        </thead>
        <c:forEach items="${gradeList}" var="e">
            <tr>
                <td>${e.stu_id}</td>
                <td>${e.stu_name}</td>
                <td>${e.exam_label}</td>
                <td>${e.score}</td>
                <td>
                    <button class="btn-outline-primary" onclick="window.location.href='/EditRecord?id=${e.id}';">修改</button>
                    <button class="btn-outline-danger" onclick="deleteGrade(${e.id})">删除</button>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="list-group">
<%--    id=0见 edu.zhangxiukai.GradeController.EditRecord--%>
    <a href="${pageContext.request.contextPath}/EditRecord?id=0" class="list-group-item list-group-item-action">添加记录</a>
</div>

</body>
<script>
    // 主要用来提示用户是否确认删除成绩记录
    function deleteGrade(id) {
        if (confirm('你确定要删除吗？')) {
            window.location.href = 'DeleteRecord?id=' + id;
        }
    }
</script>
</html>
