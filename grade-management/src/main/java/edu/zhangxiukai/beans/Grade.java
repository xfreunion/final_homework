package edu.zhangxiukai.beans;

import org.springframework.stereotype.Component;

/**
 * Grade
 * 主要的数据模型类，代表如下表的一条记录
 * create table grades
 * (
 *     id         int primary key not null unique auto_increment,
 *     exam_lable tinytext        not null, # 考试的名称, 如:2020年第一次语文月考
 *     stu_id     int             not null,
 *     stu_name   tinytext        not null,
 *     score      float           not null, # 分数百分制
 *     index (stu_id, score)
 * );
 * @author kurodamaria 张修开 191软件3
 * @since 2020-12-27
 */
@Component
public class Grade {
    int id;
    String exam_label;
    int stu_id;
    String stu_name;
    float score;

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", examLabel='" + exam_label + '\'' +
                ", stuId=" + stu_id +
                ", stuName='" + stu_name + '\'' +
                ", score=" + score +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExam_label() {
        return exam_label;
    }

    public void setExam_label(String exam_label) {
        this.exam_label = exam_label;
    }

    public int getStu_id() {
        return stu_id;
    }

    public void setStu_id(int stu_id) {
        this.stu_id = stu_id;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}