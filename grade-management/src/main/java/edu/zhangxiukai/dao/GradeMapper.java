package edu.zhangxiukai.dao;

import edu.zhangxiukai.beans.Grade;
import edu.zhangxiukai.utils.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * GradeMapper：grades表的DAO
 * grades表：
 * create table grades
 * (
 *     id         int primary key not null unique auto_increment,
 *     exam_lable tinytext        not null, # 考试的名称, 如:2020年第一次语文月考
 *     stu_id     int             not null,
 *     stu_name   tinytext        not null,
 *     score      float           not null, # 分数百分制
 *     index (stu_id, score)
 * );
 *
 * 关于getAllGrade, insertGrade, updateGrade, deleteGradeById, getGradeById请参见GradeMapper.xml
 *
 * @see edu.zhangxiukai.utils.MyBatisUtil
 * @author kurodamaria 张修开 191软件3
 * @since 2020-12-27
 */

@Repository
public class GradeMapper {
    /**
     * @return 返回从数据库获得的成绩列表
     */
    public List<Grade> getAllGrade() {
        return MyBatisUtil.runSql(
                (SqlSession sqlSession) -> sqlSession.selectList("getAllGrade")
        );
    }

    /**
     * 插入一条成绩
     * @param Grade 要插入的成绩对象
     */
    public void insertGrade(Grade Grade) {
        MyBatisUtil.runSql(
                (SqlSession sqlSession) -> sqlSession.insert("insertGrade", Grade)
        );
    }

    /**
     * 更新指定id的成绩（id在传入的grade对象里）
     * @param grade 新的成绩
     */
    public void updateGrade(Grade grade) {
        MyBatisUtil.runSql(
                (SqlSession sqlSession) -> sqlSession.insert("updateGrade", grade)
        );
    }

    /**
     * 删除一条指定id的成绩
     * @param id 要删除的成绩的id
     */
    public void deleteGrade(int id) {
        MyBatisUtil.runSql(
                (SqlSession sqlSession) -> sqlSession.delete("deleteGradeById", id)
        );
    }

    /**
     * 通过id获取一条成绩
     * @param id 要获取的成绩的id
     * @return 返回获取到的成绩
     */
    public Grade getGradeById(int id) {
        return MyBatisUtil.runSql(
                (SqlSession sqlSession) -> sqlSession.selectOne("getGradeById", id)
        );
    }
}
