package edu.zhangxiukai.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;

/**
 * MyBatisUtil
 * 工具类，方便操作数据库
 * 一般用法：
 * T ret = MyBatisUtil.runSql(
 *     (SqlSession sqlSession) -> sqlSession.xxx()
 * );
 * T为sqlSession.xxx()的返回类型
 *
 * 也可以单独获取SqlSessionFactory
 * SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();
 *
 * @author kurodamaria 张修开 191软件3
 * @since 2020-12-27
 */
public class MyBatisUtil {
    private static SqlSessionFactory sqlSessionFactory = null; // 预设为null，以防获取factory失败（看下面）

    // 获取SqlSessionFactory
    static {
        InputStream in = null;
        try {
            in = Resources.getResourceAsStream("mybatis.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            // 没有异常再赋值sqlSessionFactory
            sqlSessionFactory = builder.build(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 返回sqlSessionFactory
     * @return mybatis.xml对应的SqlSessionFactory
     */
    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    /**
     * 减去重复语句的工具函数
     * 重复语句如下
     * 1. SqlSession session = sqlSessionFactory.openSession();
     * 2. session.commit();
     * 3. session.close();
     * @param function 利用传入的SqlSession对数据库执行的操作
     * @param <T> 操作返回的对象的类型
     * @return 返回操作的结果
     */
    public static <T> T runSql(Function<SqlSession, T> function) {
        SqlSession session = sqlSessionFactory.openSession();
        T ret = function.apply(session);
        session.commit();
        session.close();
        return ret;
    }
}
