package edu.zhangxiukai.controllers;

import edu.zhangxiukai.beans.Grade;
import edu.zhangxiukai.dao.GradeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * GradeController：主要的控制器，负责页面和持久层的交互
 * @see edu.zhangxiukai.dao.GradeMapper
 * @author kurodamaria 张修开 191软件3
 * @since 2020-12-27
 */

@Controller
public class GradeController {
    @Autowired
    GradeMapper gradesMapper;   // grade表的DAO

    /**
     * 首页
     * @return 跳转到manage-grade.jsp
     */
    @RequestMapping("/")
    public ModelAndView listGrade() {
        ModelAndView mav = new ModelAndView("manage-grade");

        List<Grade> gradeList = gradesMapper.getAllGrade();

        // 在manage-grade.jsp中使用gradeList显示成绩列表
        mav.addObject("gradeList", gradeList);
        return mav;
    }


    /**
     * 插入或更新成绩
     * @param grade 要保存到数据库的成绩对象
     * @return 跳转到首页，即manage-grade.jsp
     */
    @RequestMapping("/SaveRecord")
    public String saveNewRecord(@ModelAttribute("grade") Grade grade) {
        // MySQL auto_increment 是从1开始数的，如果是新建记录，id默认为0
        if (grade.getId() == 0) {
            // 插入一条新记录
            gradesMapper.insertGrade(utf8Grade(grade));
        } else {
            // 更新鲜有记录
            gradesMapper.updateGrade(utf8Grade(grade));
        }
        return "redirect: /"; // 返回首页
    }

    /**
     * 删除成绩
     * @param id 要删除成绩的id
     * @return 跳转到主页，即manage-grade.jsp
     */
    @RequestMapping("/DeleteRecord")
    public String deleteRecord(@RequestParam("id") int id) {
        gradesMapper.deleteGrade(id);
        return "redirect: /";
    }

    /**
     * 编辑记录，可能是编辑一条新纪录或者编辑现有记录
     * @param id 要编辑的记录的id。如果是0就是新建记录，其他则是编辑指定id对应的记录，因为MySQL auto_increment从0开始数
     * @return 将要编辑的成绩对象存入mav后跳转到edit-record.jsp（负责编辑）
     */
    @RequestMapping("/EditRecord")
    public ModelAndView editRecord(@RequestParam("id") int id) {
        ModelAndView mav = new ModelAndView("edit-record");
        if (id == 0) {
            mav.addObject("grade", new Grade());
        } else {
            mav.addObject("grade", gradesMapper.getGradeById(id));
        }
        return mav;
    }

    /**
     * 网页传回来的字符是ISO_8859_1编码，这里转换成数据库的UTF8
     * @param grade 要修改编码的Grade对象
     * @return 返回修改后的grade的引用
     */
    public static Grade utf8Grade(Grade grade) {
        // 修改 exam_label为UTF8编码
        grade.setExam_label(
                new String(
                        grade.getExam_label().getBytes(StandardCharsets.ISO_8859_1),
                        StandardCharsets.UTF_8
                )
        );
        // 修改 stu_name为UTF8编码
        grade.setStu_name(
                new String(
                        grade.getStu_name().getBytes(StandardCharsets.ISO_8859_1),
                        StandardCharsets.UTF_8
                )
        );
        return grade;
    }
}
